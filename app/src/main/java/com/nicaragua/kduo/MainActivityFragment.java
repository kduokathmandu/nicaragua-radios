/**
 * MainActivityFragment.java
 * Implements the main fragment of the main activity
 * This fragment is a list view of radio stations
 * <p>
 * This file is part of
 * TRANSISTOR - Radio App for Android
 * <p>
 * Copyright (c) 2015-16 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package com.nicaragua.kduo;

import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import android.app.SearchManager;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.nicaragua.kduo.app.AppStation;
import com.nicaragua.kduo.app.MyRadio;
import com.nicaragua.kduo.helpers.LogHelper;
import com.nicaragua.kduo.app.CustomStation;
import com.nicaragua.kduo.core.Station;
import com.nicaragua.kduo.helpers.PermissionHelper;
import com.nicaragua.kduo.helpers.SleepTimerService;
import com.nicaragua.kduo.helpers.TransistorKeys;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


/**
 * MainActivityFragment class
 */
public final class MainActivityFragment extends Fragment implements SearchView.OnQueryTextListener {


    // A Native Express ad is placed in every nth position in the RecyclerView.
    public static final int ITEMS_PER_AD = 8;
    public static int START_AD_ITEM = 300;

    // The Native Express ad height.
    private static final int NATIVE_EXPRESS_AD_HEIGHT = 85;

    // The Native Express ad unit ID.
    private static final String AD_UNIT_ID = "ca-app-pub-3231405590589504/9453779500";


    /* Define log tag */
    private static final String LOG_TAG = MainActivityFragment.class.getSimpleName();
    public static String CURRENT_RADIO = "current_radio";
    //    public AdView adView;
    private ArrayList<Object> customStations;
    /* Main class variables */
    private Application mApplication;
    private Activity mActivity;
    private CollectionAdapter mCollectionAdapter = null;
    private File mFolder;
    private int mFolderSize;
    private View mRootView;
    private View mActionCallView;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private Parcelable mListState;
    private BroadcastReceiver mCollectionChangedReceiver;
    private BroadcastReceiver mImageChangeRequestReceiver;
    private BroadcastReceiver mSleepTimerStartedReceiver;
    private BroadcastReceiver mPlaybackStateChangedReceiver;
    private int mStationIDSelected;
    private int mStationIDCurrent;
    private int mStationIDLast;
    private String currentPlayingStation;
    private int mTempStationID;
    private Station mTempStation;
    private Uri mNewStationUri;
    private boolean mTwoPane;
    private boolean mPlayback;
    private boolean mSleepTimerRunning;
    private SleepTimerService mSleepTimerService;
    private String mSleepTimerNotificationMessage;
    private Snackbar mSleepTimerNotification;
    private TextView stationStatus;
    private ImageView stationStatusImage;
    private ActionBar mActionBar;
    private SearchView mSearchView;
    private boolean reversed = false;
    private boolean shoFavourite = false;
    private SharedPreferences prefs;
    private MyRadio myRadio;
    private Menu menu;


    /* Constructor (default) */
    public MainActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        mActionBar = ((MainActivity) getActivity()).actionBar;

        myRadio = MyRadio.getInstance();


        // fragment has options menu
        setHasOptionsMenu(true);

        // get activity and application contexts
        mActivity = getActivity();
        mApplication = mActivity.getApplication();

        prefs = mActivity.getSharedPreferences("com.suman.radio.app.tms", Context.MODE_PRIVATE);

        // get notification message
        mSleepTimerNotificationMessage = mActivity.getString(com.nicaragua.kduo.R.string.snackbar_message_timer_set) + " ";

        // initiate sleep timer service
        mSleepTimerService = new SleepTimerService();

        // set list state null
        mListState = null;

        // initialize id of currently selected station
        mStationIDSelected = 0;

        // initialize temporary station image id
        mTempStationID = -1;

        // initialize two pane
        mTwoPane = false;

        // load playback state
        loadAppState(mActivity);

        // create collection adapter
        if (mCollectionAdapter == null) {


            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference("stations/nicaragua_radios");


            // Read from the database
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {


                    customStations = new ArrayList<Object>();

                    String[] favourites = getFravouteStation();

                    for (DataSnapshot station : dataSnapshot.getChildren()) {

                        AppStation s = station.getValue(AppStation.class);

                        if (s != null) {

                            if (Arrays.asList(favourites).contains(s.getRadio_title())) {
                                customStations.add(new Station(new CustomStation(s.getRadio_title(), s.getStream_url(), s.getImage_url(), true)));
                            } else {
                                customStations.add(new Station(new CustomStation(s.getRadio_title(), s.getStream_url(), s.getImage_url())));
                            }


                        }
                    }


                    ArrayList<Station> shortingStation = new ArrayList<Station>();
                    for (Object stn : customStations) {

                        Station thisStation = (Station) stn;
                        shortingStation.add(thisStation);


                    }

                    Collections.sort(shortingStation, new Comparator<Station>() {
                        @Override
                        public int compare(Station station, Station t1) {
                            return station.getStationName().compareToIgnoreCase(t1.getStationName());
                        }
                    });

                    customStations = new ArrayList<Object>();
                    for (Station stn : shortingStation) {
                        Object nweObj = (Object) stn;
                        customStations.add(nweObj);
                    }

//                    Collections.reverse(customStations);


                    mCollectionAdapter = new CollectionAdapter(mActivity, customStations);

                    addNativeExpressAds();
                    setUpAndLoadNativeExpressAds();

                    mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                    mRecyclerView.setLayoutManager(mLayoutManager);
                    mRecyclerView.setAdapter(mCollectionAdapter);

//                    Collections.reverse(customStations);
                    mCollectionAdapter.notifyDataSetChanged();
                    toggleActionCall();
                    setLoadingView();

                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    Log.w("TAG", "Failed to read value.", error.toException());
                }
            });


        }


        // initialize broadcast receivers
        initializeBroadcastReceivers();

    }

    public AdView adView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // get list state from saved instance
        if (savedInstanceState != null) {
            mListState = savedInstanceState.getParcelable(TransistorKeys.INSTANCE_LIST_STATE);
        }

        // inflate root view from xml
        mRootView = inflater.inflate(com.nicaragua.kduo.R.layout.fragment_main, container, false);

        adView = (AdView) mRootView.findViewById(com.nicaragua.kduo.R.id.myAds);


        AdRequest mAdRequest = new AdRequest.Builder()
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();

        adView.loadAd(mAdRequest);


        // get reference to action call view from inflated root view
        mActionCallView = mRootView.findViewById(com.nicaragua.kduo.R.id.main_actioncall_layout);

        stationStatus = (TextView) mRootView.findViewById(com.nicaragua.kduo.R.id.TV_chooseStation);
        stationStatusImage = (ImageView) mRootView.findViewById(com.nicaragua.kduo.R.id.IV_musicBarIcon);

        mSearchView = (SearchView) mRootView.findViewById(com.nicaragua.kduo.R.id.SV_serach);
        SearchManager manager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchView.setSearchableInfo(manager.getSearchableInfo(getActivity().getComponentName()));
        mSearchView.setIconifiedByDefault(true);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                return true;
            }
        });


        // get reference to recycler list view from inflated root view
        mRecyclerView = (RecyclerView) mRootView.findViewById(com.nicaragua.kduo.R.id.main_recyclerview_collection);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        // TODO check if necessary here
        mRecyclerView.setHasFixedSize(true);

        // set animator
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // attach adapter to list view
        mRecyclerView.setAdapter(mCollectionAdapter);


//        toggleActionCall();
        return mRootView;
    }

    private void setLoadingView() {


        if (customStations != null && mPlayback) {

            Station nStation = null;
            int currentStation = 0;
//            String currentRadio = prefs.getString(CURRENT_RADIO, "false--");

            String currentRadio = currentPlayingStation;

            for (int i = 0; i < customStations.size(); i++) {

                if (customStations.get(i) instanceof Station) {
                    Station stnn = (Station) customStations.get(i);

                    if (stnn.getPlaybackState()) {
                        nStation = stnn;
                        currentStation = i;
                    }
                    if (currentRadio.equals(stnn.getStationName())) {
                        nStation = stnn;
                        currentStation = i;
                    }

                }

            }


            if (nStation != null) {

                prefs.edit().putString(CURRENT_RADIO, nStation.getStationName()).apply();
                stationStatus.setText("Now playing: " + nStation.getStationName());

                stationStatusImage.setBackgroundResource(com.nicaragua.kduo.R.drawable.music_bar);
                Picasso.with(mActivity).load(com.nicaragua.kduo.R.drawable.music_bar).fit().placeholder(com.nicaragua.kduo.R.drawable.music_bar).into(stationStatusImage);

                final Station finalNStation = nStation;
                final int finalCurrentStation = currentStation;
                stationStatus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        if (mTwoPane) {
//                            Bundle args = new Bundle();
//                            args.putParcelable(TransistorKeys.ARG_STATION, finalNStation);
//                            args.putInt(TransistorKeys.ARG_STATION_ID, finalCurrentStation);
//                            args.putBoolean(TransistorKeys.ARG_TWO_PANE, mTwoPane);
//
//                            PlayerActivityFragment playerActivityFragment = new PlayerActivityFragment();
//                            playerActivityFragment.setArguments(args);
//                            mActivity.getFragmentManager().beginTransaction()
//                                    .replace(R.id.player_container, playerActivityFragment, TransistorKeys.PLAYER_FRAGMENT_TAG)
//                                    .commit();
//                        } else {
                        // add ID of station to intent and start activity
                        showAds();
                        Intent intent = new Intent(mActivity, PlayerActivity.class);
                        intent.setAction(TransistorKeys.ACTION_SHOW_PLAYER);
                        intent.putExtra(TransistorKeys.EXTRA_STATION, finalNStation);
                        intent.putExtra(TransistorKeys.EXTRA_STATION_ID, finalCurrentStation);
                        mActivity.startActivity(intent);
//                        }
                    }
                });


            }
        }
    }

    private void showAds() {
        if (myRadio.mInterstitialAd.isLoaded()) {
            myRadio.mInterstitialAd.show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (customStations != null) {
            loadAppState(mActivity);
            setLoadingView();
        }


        // handles the activity's intent
        Intent intent = mActivity.getIntent();
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            handleStreamingLink(intent);
        } else if (TransistorKeys.ACTION_SHOW_PLAYER.equals(intent.getAction())) {
            handleShowPlayer(intent);
        }


        if (mCollectionAdapter != null) {
            // refresh app state
            loadAppState(mActivity);

            // update collection adapter
            mCollectionAdapter.setTwoPane(mTwoPane);
            mCollectionAdapter.refresh();
            if (mCollectionAdapter.getItemCount() > 0) {
                if (customStations.get(mStationIDSelected) instanceof Station) {
                    mCollectionAdapter.setStationIDSelected(mStationIDSelected, mPlayback, false);
                }
            }


            // show call to action, if necessary
            toggleActionCall();

            // show notification bar if timer is running
            if (mSleepTimerRunning) {
                showSleepTimerNotification(-1);
            }
        }
    }

    @Override
    public void onDestroy() {
//        if (adView != null) {
//            adView.destroy();
//        }
        super.onDestroy();
        unregisterBroadcastReceivers();
        prefs.edit().putString(CURRENT_RADIO, "false--").apply();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                if (reversed) {
                    reversed = false;
                    mActionBar.setHomeAsUpIndicator(com.nicaragua.kduo.R.drawable.favourite_menu_false);
                } else {
                    reversed = true;
                    mActionBar.setHomeAsUpIndicator(com.nicaragua.kduo.R.drawable.favourite_menu_true);
                }
                if (mCollectionAdapter != null)
                    mCollectionAdapter.filterFavourites(reversed);


                return true;

            // CASE TIMER
            case com.nicaragua.kduo.R.id.menu_timer:

                final Calendar now = Calendar.getInstance();


                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Dialog, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        String chosenTime = convertTo12Hour(hourOfDay + "", minute + "");
                        String currentTime = convertTo12Hour(now.get(Calendar.HOUR_OF_DAY) + "", now.get(Calendar.MINUTE) + "");

                        if (checkTimeDifference(chosenTime, currentTime)) {
                            Toast.makeText(getActivity(), "Selected time must be greater current time! ", Toast.LENGTH_SHORT).show();
                        } else {

                            Calendar thatTime = Calendar.getInstance();
                            thatTime.set(Calendar.DAY_OF_MONTH, now.get(Calendar.DAY_OF_MONTH));
                            thatTime.set(Calendar.MONTH, now.get(Calendar.MONTH)); // 0-11 so 1 less
                            thatTime.set(Calendar.YEAR, now.get(Calendar.YEAR));
                            thatTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            thatTime.set(Calendar.MINUTE, minute);

                            Calendar today = Calendar.getInstance();

                            long timeDifference = thatTime.getTimeInMillis() - today.getTimeInMillis();
                            handleMenuSleepTimerClick(timeDifference);
                        }


                    }
                }, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), false);
                timePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                timePickerDialog.show();

//                TimePickerDialog timePickerDialog = new TimePickerDialog(
//                        getActivity(), new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker timePicker, int _hour, int _minutes) {
//
//                        long current_hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
//                        long current_minutes = Calendar.getInstance().get(Calendar.MINUTE);
//
//                        long set_minutes = TimeUnit.MINUTES.toMillis(_minutes);
//                        long set_hour = TimeUnit.HOURS.toMillis(_hour);
//                        long total = (current_hours + current_minutes) - (set_hour + set_minutes);
//                        Log.e("Test", "test");
//                        Log.e("Test", "test");
//                        Toast.makeText(mApplication, "starts in: " + total / 1000, Toast.LENGTH_SHORT).show();
////                        handleMenuSleepTimerClick(total);
//                    }
//                }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), false);
//
////
//                timePickerDialog.show();

                return true;

            case com.nicaragua.kduo.R.id.menu_short:


                if (customStations != null) {
                    if (reversed) {
                        reversed = false;
                    } else {
                        reversed = true;
                    }
                    if (mCollectionAdapter != null) {
                        mCollectionAdapter.sortStations();
                        menu.getItem(0).setIcon(ContextCompat.getDrawable(getActivity(), (reversed) ? com.nicaragua.kduo.R.drawable.sort_z_to_a : com.nicaragua.kduo.R.drawable.sort_a_to_z));
                    }
                }
                return true;

            case com.nicaragua.kduo.R.id.menu_about:
                // get title and content
                String aboutTitle = mActivity.getString(com.nicaragua.kduo.R.string.header_about);
                // put title and content into intent and start activity
                Intent aboutIntent = new Intent(mActivity, InfosheetActivity.class);
                aboutIntent.putExtra(TransistorKeys.EXTRA_INFOSHEET_TITLE, aboutTitle);
                aboutIntent.putExtra(TransistorKeys.EXTRA_INFOSHEET_CONTENT, TransistorKeys.INFOSHEET_CONTENT_ABOUT);
                startActivity(aboutIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // save list view position
        mListState = mLayoutManager.onSaveInstanceState();
        outState.putParcelable(TransistorKeys.INSTANCE_LIST_STATE, mListState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case TransistorKeys.PERMISSION_REQUEST_IMAGE_PICKER_READ_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted - get system picker for images
                    Intent pickImageIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    mActivity.startActivityForResult(pickImageIntent, TransistorKeys.REQUEST_LOAD_IMAGE);
                } else {
                    // permission denied
                    Toast.makeText(mActivity, mActivity.getString(com.nicaragua.kduo.R.string.toastalert_permission_denied) + " READ_EXTERNAL_STORAGE", Toast.LENGTH_LONG).show();
                }
                break;
            }

            case TransistorKeys.PERMISSION_REQUEST_STATION_FETCHER_READ_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted - fetch station from given Uri
//                    fetchNewStation(mNewStationUri);
                } else {
                    // permission denied
                    Toast.makeText(mActivity, mActivity.getString(com.nicaragua.kduo.R.string.toastalert_permission_denied) + " READ_EXTERNAL_STORAGE", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // retrieve selected image Uri from image picker
        Uri newImageUri = null;
        if (null != data) {
            newImageUri = data.getData();
        }

        if (requestCode == TransistorKeys.REQUEST_LOAD_IMAGE && resultCode == Activity.RESULT_OK && newImageUri != null) {

/*            ImageHelper imageHelper = new ImageHelper(newImageUri, mActivity);
            Bitmap newImage = imageHelper.getInputImage();

            if (newImage != null && mTempStationID != -1) {
                // write image to storage
                File stationImageFile = mTempStation.getStationImageFile();
                try (FileOutputStream out = new FileOutputStream(stationImageFile)) {
                    newImage.compress(Bitmap.CompressFormat.PNG, 100, out);
                } catch (IOException e) {
                    LogHelper.e(LOG_TAG, "Unable to save: " + newImage.toString());
                }
                // update adapter
                mCollectionAdapter.notifyItemChanged(mTempStationID);

            } else {
                LogHelper.e(LOG_TAG, "Unable to get image from media picker. Uri was:  " + newImageUri.toString());
            }*/

        } else {
            LogHelper.e(LOG_TAG, "Unable to get image from media picker. Did not receive an Uri");
        }
    }

    /* Show or hide call to action view if necessary */
    private void toggleActionCall() {
        // show call to action, if necessary
        if (customStations != null && customStations.size() == 0) {
            mActionCallView.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            mSearchView.setVisibility(View.GONE);
        } else {
            mActionCallView.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            mSearchView.setVisibility(View.VISIBLE);
        }
    }

    /* Check permissions and start image picker */
    private void selectFromImagePicker() {
        // request read permissions
        PermissionHelper permissionHelper = new PermissionHelper(mActivity, mRootView);
        if (permissionHelper.requestReadExternalStorage(TransistorKeys.PERMISSION_REQUEST_IMAGE_PICKER_READ_EXTERNAL_STORAGE)) {
            // get system picker for images
            Intent pickImageIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickImageIntent, TransistorKeys.REQUEST_LOAD_IMAGE);
        }
    }

    /* Handles tap on streaming link */
    private void handleStreamingLink(Intent intent) {
        mNewStationUri = intent.getData();

        // clear the intent
        intent.setAction("");

        // check for null and type "http"
        if (mNewStationUri != null && mNewStationUri.getScheme().startsWith("http")) {
            // download and add new station
//            fetchNewStation(mNewStationUri);
        } else if (mNewStationUri != null && mNewStationUri.getScheme().startsWith("file")) {
            // check for read permission
            PermissionHelper permissionHelper = new PermissionHelper(mActivity, mRootView);
            if (permissionHelper.requestReadExternalStorage(TransistorKeys.PERMISSION_REQUEST_STATION_FETCHER_READ_EXTERNAL_STORAGE)) {
                // read and add new station
//                fetchNewStation(mNewStationUri);
            }
        }
        // unsuccessful - log failure
        else {
            LogHelper.v(LOG_TAG, "Received an empty intent");
        }
    }

    /* Handles intent to show player from notification or from shortcut */
    private void handleShowPlayer(Intent intent) {
        // get station from intent
        Station station = null;
        if (intent.hasExtra(TransistorKeys.EXTRA_STATION)) {
            // get station from notification
            station = intent.getParcelableExtra(TransistorKeys.EXTRA_STATION);
            prefs.edit().putString(CURRENT_RADIO, station.getStationName()).apply();

        } else if (intent.hasExtra(TransistorKeys.EXTRA_STREAM_URI)) {
            // get Uri of station from home screen shortcut
            station = mCollectionAdapter.findStation(Uri.parse(intent.getStringExtra(TransistorKeys.EXTRA_STREAM_URI)));
        } else if (intent.hasExtra(TransistorKeys.EXTRA_LAST_STATION) && intent.getBooleanExtra(TransistorKeys.EXTRA_LAST_STATION, false)) {
            // try to get last station
            loadAppState(mActivity);
            if (mStationIDLast > -1 && mStationIDLast < mCollectionAdapter.getItemCount()) {
                station = (Station) mCollectionAdapter.getStation(mStationIDLast);
            }
        }

        if (station == null) {
            Toast.makeText(mActivity, getString(com.nicaragua.kduo.R.string.toastalert_station_not_found), Toast.LENGTH_LONG).show();
        }

        // get playback action from intent
        boolean startPlayback;
        if (intent.hasExtra(TransistorKeys.EXTRA_PLAYBACK_STATE)) {
            startPlayback = intent.getBooleanExtra(TransistorKeys.EXTRA_PLAYBACK_STATE, false);
        } else {
            startPlayback = false;
        }


        showAds();
        Intent playerIntent = new Intent(mActivity, PlayerActivity.class);
        playerIntent.setAction(TransistorKeys.ACTION_SHOW_PLAYER);
        playerIntent.putExtra(TransistorKeys.EXTRA_STATION, station);
        playerIntent.putExtra(TransistorKeys.EXTRA_PLAYBACK_STATE, startPlayback);
        startActivity(playerIntent);
//        }
    }

    /* Handles tap timer icon in actionbar */
    private void handleMenuSleepTimerClick(long duration) {
        // load app state
        loadAppState(mActivity);

        // set duration
//        long duration = 900000; // equals 15 minutes

        // CASE: No station is playing, no timer is running
        if (!mPlayback && !mSleepTimerRunning) {
            // unable to start timer
            Toast.makeText(mActivity, mActivity.getString(com.nicaragua.kduo.R.string.toastmessage_timer_start_unable), Toast.LENGTH_SHORT).show();
        }
        // CASE: A station is playing, no sleep timer is running
        else if (mPlayback && !mSleepTimerRunning) {
            startSleepTimer(duration);
            Toast.makeText(mActivity, mActivity.getString(com.nicaragua.kduo.R.string.toastmessage_timer_activated), Toast.LENGTH_SHORT).show();
        }
        // CASE: A station is playing, Sleep timer is running
        else if (mPlayback) {
            startSleepTimer(duration);
            Toast.makeText(mActivity, mActivity.getString(com.nicaragua.kduo.R.string.toastmessage_timer_duration_increased) + " [+" + getReadableTime(duration) + "]", Toast.LENGTH_SHORT).show();
        }

    }

    /* Starts timer service and notification */
    private void startSleepTimer(long duration) {
        // start timer service
        if (mSleepTimerService == null) {
            mSleepTimerService = new SleepTimerService();
        }
        mSleepTimerService.startActionStart(mActivity, duration);

        // show timer notification
        showSleepTimerNotification(duration);
        mSleepTimerRunning = true;
        LogHelper.v(LOG_TAG, "Starting timer service and notification.");
    }

    /* Stops timer service and notification */
    private void stopSleepTimer() {
        // stop timer service
        if (mSleepTimerService != null) {
            mSleepTimerService.startActionStop(mActivity);
        }
        // cancel notification
        if (mSleepTimerNotification != null && mSleepTimerNotification.isShown()) {
            mSleepTimerNotification.dismiss();
        }
        mSleepTimerRunning = false;
        LogHelper.v(LOG_TAG, "Stopping timer service and notification.");
        Toast.makeText(mActivity, mActivity.getString(com.nicaragua.kduo.R.string.toastmessage_timer_cancelled), Toast.LENGTH_SHORT).show();
    }

    /* Shows notification for a running sleep timer */
    private void showSleepTimerNotification(long remainingTime) {

        // set snackbar message
        String message;
        if (remainingTime > 0) {
            message = mSleepTimerNotificationMessage + getReadableTime(remainingTime);
        } else {
            message = mSleepTimerNotificationMessage;
        }

        // show snackbar
        mSleepTimerNotification = Snackbar.make(mRootView, message, Snackbar.LENGTH_INDEFINITE);
        mSleepTimerNotification.setAction(com.nicaragua.kduo.R.string.dialog_generic_button_cancel, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // stop sleep timer service
                mSleepTimerService.startActionStop(mActivity);
                mSleepTimerRunning = false;
                saveAppState(mActivity);
                // notify user
                Toast.makeText(mActivity, mActivity.getString(com.nicaragua.kduo.R.string.toastmessage_timer_cancelled), Toast.LENGTH_SHORT).show();
                LogHelper.v(LOG_TAG, "Sleep timer cancelled.");
            }
        });
        mSleepTimerNotification.show();

    }

    /* Translates milliseconds into minutes and seconds */
    private String getReadableTime(long remainingTime) {
        return String.format(Locale.getDefault(), "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(remainingTime),
                TimeUnit.MILLISECONDS.toSeconds(remainingTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(remainingTime)));
    }

    /* Loads app state from preferences */
    private void loadAppState(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        mStationIDSelected = settings.getInt(TransistorKeys.PREF_STATION_ID_SELECTED, 0);
        mStationIDCurrent = settings.getInt(TransistorKeys.PREF_STATION_ID_CURRENTLY_PLAYING, -1);
        mStationIDLast = settings.getInt(TransistorKeys.PREF_STATION_ID_LAST, -1);
        currentPlayingStation = settings.getString(TransistorKeys.PREF_STATION_NAME_CURRENTLY_PLAYING, "Choose a station below to begin..");
        mPlayback = settings.getBoolean(TransistorKeys.PREF_PLAYBACK, false);
        mTwoPane = settings.getBoolean(TransistorKeys.PREF_TWO_PANE, false);
        mSleepTimerRunning = settings.getBoolean(TransistorKeys.PREF_TIMER_RUNNING, false);
        LogHelper.v(LOG_TAG, "Loading state (" + mStationIDCurrent + " / " + mStationIDLast + " / " + mPlayback + ")");
    }

    /* Saves app state to SharedPreferences */
    private void saveAppState(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(TransistorKeys.PREF_STATION_ID_CURRENTLY_PLAYING, mStationIDCurrent);
        editor.putInt(TransistorKeys.PREF_STATION_ID_LAST, mStationIDLast);
        editor.putString(TransistorKeys.PREF_STATION_NAME_CURRENTLY_PLAYING, currentPlayingStation);
        editor.putBoolean(TransistorKeys.PREF_PLAYBACK, mPlayback);
        editor.putBoolean(TransistorKeys.PREF_TIMER_RUNNING, mSleepTimerRunning);
        editor.apply();
        LogHelper.v(LOG_TAG, "Saving state (" + mStationIDCurrent + " / " + mStationIDLast + " / " + mPlayback + ")");
    }

    private String[] getFravouteStation() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mActivity);
        String[] tempPlayList = settings.getString(TransistorKeys.PREF_FAVORITE_STATION, "").split(",");
        return tempPlayList;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        this.menu = menu;
    }

    /* Initializes broadcast receivers for onCreate */
    private void initializeBroadcastReceivers() {

        // RECEIVER: state of playback has changed
        mPlaybackStateChangedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE)) {
                    handlePlaybackStateChanges(intent);
                }
            }
        };
        IntentFilter playbackStateChangedIntentFilter = new IntentFilter(TransistorKeys.ACTION_PLAYBACK_STATE_CHANGED);
        LocalBroadcastManager.getInstance(mActivity).registerReceiver(mPlaybackStateChangedReceiver, playbackStateChangedIntentFilter);

        // RECEIVER: station added, deleted, or changed
        mCollectionChangedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null && intent.hasExtra(TransistorKeys.EXTRA_COLLECTION_CHANGE)) {
//                    handleCollectionChanges(intent);
                }
            }
        };
        IntentFilter collectionChangedIntentFilter = new IntentFilter(TransistorKeys.ACTION_COLLECTION_CHANGED);
        LocalBroadcastManager.getInstance(mApplication).registerReceiver(mCollectionChangedReceiver, collectionChangedIntentFilter);

        // RECEIVER: listen for request to change station image
        mImageChangeRequestReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra(TransistorKeys.EXTRA_STATION) && intent.hasExtra(TransistorKeys.EXTRA_STATION_ID)) {
                    // get station and id from intent
                    mTempStation = intent.getParcelableExtra(TransistorKeys.EXTRA_STATION);
                    mTempStationID = intent.getIntExtra(TransistorKeys.EXTRA_STATION_ID, -1);
                    // start image picker
                    selectFromImagePicker();
                }
            }
        };
        IntentFilter imageChangeRequestIntentFilter = new IntentFilter(TransistorKeys.ACTION_IMAGE_CHANGE_REQUESTED);
        LocalBroadcastManager.getInstance(mApplication).registerReceiver(mImageChangeRequestReceiver, imageChangeRequestIntentFilter);

        // RECEIVER: sleep timer service sends updates
        mSleepTimerStartedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // get duration from intent
                long remaining = intent.getLongExtra(TransistorKeys.EXTRA_TIMER_REMAINING, 0);
                if (mSleepTimerNotification != null && remaining > 0) {
                    // update existing notification
                    mSleepTimerNotification.setText(mSleepTimerNotificationMessage + getReadableTime(remaining));
                } else if (mSleepTimerNotification != null) {
                    // cancel notification
                    mSleepTimerNotification.dismiss();
                    // save state and update user interface
                    mPlayback = false;
                    mSleepTimerRunning = false;
                    saveAppState(mActivity);
                }

            }
        };
        IntentFilter sleepTimerIntentFilter = new IntentFilter(TransistorKeys.ACTION_TIMER_RUNNING);
        LocalBroadcastManager.getInstance(mActivity).registerReceiver(mSleepTimerStartedReceiver, sleepTimerIntentFilter);

    }


    /* Unregisters broadcast receivers */
    private void unregisterBroadcastReceivers() {
        LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mPlaybackStateChangedReceiver);
        LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mCollectionChangedReceiver);
        LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mImageChangeRequestReceiver);
        LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mSleepTimerStartedReceiver);
    }

    /* Handles changes in state of playback, eg. start, stop, loading stream */
    private void handlePlaybackStateChanges(Intent intent) {
        switch (intent.getIntExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE, 1)) {
            // CASE: playback was stopped
            case TransistorKeys.PLAYBACK_STOPPED:
                // load app state
                loadAppState(mActivity);
                // stop sleep timer
                if (mSleepTimerRunning && mSleepTimerService != null) {
                    stopSleepTimer();
                }
                break;
        }
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            if (mCollectionAdapter != null)
                mCollectionAdapter.getFilter().filter("");
        } else {
            if (mCollectionAdapter != null)
                mCollectionAdapter.getFilter().filter(newText.toString());
        }
        return true;
    }


    private boolean checkTimeDifference(String chosenTime, String currentTime) {


        DateFormat f1 = new SimpleDateFormat("h:mm a");


        Date __startTime = null;
        Date __endTime = null;
        try {
            __startTime = f1.parse(currentTime);
            __endTime = f1.parse(chosenTime);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (__startTime != null && __endTime != null) {
            if (__endTime.before(__startTime) || __startTime.equals(__endTime)) {
                return true;
            }
        } else {
            return true;
        }
        return false;

    }


    private String convertTo12Hour(String hourOfDay, String minueOfDay) {


        String selectedDate = "11:00 AM";
        try {
            String s = hourOfDay + ":" + minueOfDay + ":00";
            DateFormat f1 = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
            Date d = f1.parse(s);
            DateFormat f2 = new SimpleDateFormat("h:mm a");
            selectedDate = f2.format(d).toUpperCase();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return selectedDate;

    }


    private void addNativeExpressAds() {

        // Loop through the items array and place a new Native Express ad in every ith position in
        // the items List.
        for (int i = START_AD_ITEM; i <= customStations.size(); i += ITEMS_PER_AD) {
            Activity thisActivity = getActivity();
            if (thisActivity != null) {
                final NativeExpressAdView adView = new NativeExpressAdView(getActivity());
                customStations.add(i, adView);
            }

        }
    }

/*    private void addNativeExpressAds() {

        // Loop through the items array and place a new Native Express ad in every ith position in
        // the items List.
        for (int i = 0; i <= customStations.size(); i += ITEMS_PER_AD) {
            final NativeExpressAdView adView = new NativeExpressAdView(getActivity());
            customStations.add(i, adView);
        }
    }*/

    /**
     * Sets up and loads the Native Express ads.
     */
    private void setUpAndLoadNativeExpressAds() {
        // Use a Runnable to ensure that the RecyclerView has been laid out before setting the
        // ad size for the Native Express ad. This allows us to set the Native Express ad's
        // width to match the full width of the RecyclerView.
        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                final float scale = myRadio.getResources().getDisplayMetrics().density;
                // Set the ad size and ad unit ID for each Native Express ad in the items list.

                for (int i = 0; i < customStations.size(); i++) {
                    if (customStations.get(i) instanceof NativeExpressAdView) {
                        final NativeExpressAdView adView =
                                (NativeExpressAdView) customStations.get(i);
//                    final CardView cardView = (CardView) findViewById(R.id.ad_card_view);
                        CardView cardView = (CardView) getActivity().findViewById(com.nicaragua.kduo.R.id.ad_card_view);
                        if (cardView != null) {
                            final int adWidth = cardView.getWidth() - cardView.getPaddingLeft()
                                    - cardView.getPaddingRight();
                            AdSize adSize = new AdSize((int) (adWidth / scale), NATIVE_EXPRESS_AD_HEIGHT);
                            adView.setAdSize(adSize);
                        } else {

                            DisplayMetrics displaymetrics = new DisplayMetrics();
                            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
                            int height = displaymetrics.heightPixels;
                            int width = displaymetrics.widthPixels;

                            final int adWidth = width - 20;
                            AdSize adSize = new AdSize((int) (adWidth / scale), NATIVE_EXPRESS_AD_HEIGHT);
                            adView.setAdSize(adSize);

                        }
                        adView.setAdUnitId(AD_UNIT_ID);
                    }
                }

                for (int i = 0; i < customStations.size(); i++) {
                    if (customStations.get(i) instanceof NativeExpressAdView) {
                        START_AD_ITEM = i;
                        // Load the first Native Express ad in the items list.
                        loadNativeExpressAd(START_AD_ITEM);
                        return;
                    }
                }

            }
        });
    }


//    /**
//     * Sets up and loads the Native Express ads.
//     */
//    private void setUpAndLoadNativeExpressAds() {
//        // Use a Runnable to ensure that the RecyclerView has been laid out before setting the
//        // ad size for the Native Express ad. This allows us to set the Native Express ad's
//        // width to match the full width of the RecyclerView.
//        mRecyclerView.post(new Runnable() {
//            @Override
//            public void run() {
//                final float scale = getActivity().getResources().getDisplayMetrics().density;
//                // Set the ad size and ad unit ID for each Native Express ad in the items list.
//
//                for (int i = 0; i <= customStations.size(); i += ITEMS_PER_AD) {
//                    final NativeExpressAdView adView =
//                            (NativeExpressAdView) customStations.get(i);
////                    final CardView cardView = (CardView) findViewById(R.id.ad_card_view);
//                    final CardView cardView = (CardView) getActivity().findViewById(R.id.ad_card_view);
//                    final int adWidth = cardView.getWidth() - cardView.getPaddingLeft()
//                            - cardView.getPaddingRight();
//                    AdSize adSize = new AdSize((int) (adWidth / scale), NATIVE_EXPRESS_AD_HEIGHT);
//                    adView.setAdSize(adSize);
//                    adView.setAdUnitId(AD_UNIT_ID);
//                }
//
//                // Load the first Native Express ad in the items list.
//                loadNativeExpressAd(0);
//            }
//        });
//    }


    /**
     * Loads the Native Express ads in the items list.
     */
    private void loadNativeExpressAd(final int index) {

        if (index >= customStations.size()) {
            return;
        }

        Object item = customStations.get(index);
        if (item instanceof NativeExpressAdView) {
            if (!(item instanceof NativeExpressAdView)) {
                throw new ClassCastException("Expected item at index " + index + " to be a Native"
                        + " Express ad.");
            }

            final NativeExpressAdView adView = (NativeExpressAdView) item;

            // Set an AdListener on the NativeExpressAdView to wait for the previous Native Express ad
            // to finish loading before loading the next ad in the items list.
            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    // The previous Native Express ad loaded successfully, call this method again to
                    // load the next ad in the items list.
                    for (int i = index; i < customStations.size(); ++i) {
                        if (customStations.get(i) instanceof NativeExpressAdView) {
                            loadNativeExpressAd(++i);
                            return;

                        }
                    }
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    // The previous Native Express ad failed to load. Call this method again to load
                    // the next ad in the items list.
                    Log.e("MainActivity", "The previous Native Express ad failed to load. Attempting to"
                            + " load the next Native Express ad in the items list.");
//                    loadNativeExpressAd(index + ITEMS_PER_AD);

                    for (int i = index; i < customStations.size(); ++i) {
                        if (customStations.get(i) instanceof NativeExpressAdView) {
                            loadNativeExpressAd(++i);
                            return;
                        }
                    }
                }
            });

            // Load the Native Express ad.
            adView.loadAd(new AdRequest.Builder().build());
        } else {
            int i = index;
            if (i < customStations.size())
                loadNativeExpressAd(++i);
        }
    }
//
//    /**
//     * Loads the Native Express ads in the items list.
//     */
//    private void loadNativeExpressAd(final int index) {
//
//        if (index >= customStations.size()) {
//            return;
//        }
//
//        Object item = customStations.get(index);
//        if (!(item instanceof NativeExpressAdView)) {
//            throw new ClassCastException("Expected item at index " + index + " to be a Native"
//                    + " Express ad.");
//        }
//
//        final NativeExpressAdView adView = (NativeExpressAdView) item;
//
//        // Set an AdListener on the NativeExpressAdView to wait for the previous Native Express ad
//        // to finish loading before loading the next ad in the items list.
//        adView.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                super.onAdLoaded();
//                // The previous Native Express ad loaded successfully, call this method again to
//                // load the next ad in the items list.
//                loadNativeExpressAd(index + ITEMS_PER_AD);
//            }
//
//            @Override
//            public void onAdFailedToLoad(int errorCode) {
//                // The previous Native Express ad failed to load. Call this method again to load
//                // the next ad in the items list.
//                Log.e("MainActivity", "The previous Native Express ad failed to load. Attempting to"
//                        + " load the next Native Express ad in the items list.");
//                loadNativeExpressAd(index + ITEMS_PER_AD);
//            }
//        });
//
//        // Load the Native Express ad.
//        adView.loadAd(new AdRequest.Builder().build());
//    }


}
