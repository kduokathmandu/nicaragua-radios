/**
 * PlayerService.java
 * Implements the app's playback background service
 * The player service plays streaming audio
 * <p>
 * This file is part of
 * TRANSISTOR - Radio App for Android
 * <p>
 * Copyright (c) 2015-16 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package com.nicaragua.kduo;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.DebugTextViewHelper;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Util;
import com.nicaragua.kduo.app.MyRadio;
import com.nicaragua.kduo.core.Station;
import com.nicaragua.kduo.helpers.EventLogger;
import com.nicaragua.kduo.helpers.LogHelper;
import com.nicaragua.kduo.helpers.NotificationHelper;
import com.nicaragua.kduo.helpers.TrackSelectionHelper;
import com.nicaragua.kduo.helpers.TransistorKeys;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;


/**
 * PlayerService class
 */
public final class PlayerService extends Service implements ExoPlayer.EventListener,
        PlaybackControlView.VisibilityListener {

    public static final String DRM_SCHEME_UUID_EXTRA = "drm_scheme_uuid";
    public static final String DRM_LICENSE_URL = "drm_license_url";
    public static final String DRM_KEY_REQUEST_PROPERTIES = "drm_key_request_properties";
    public static final String PREFER_EXTENSION_DECODERS = "prefer_extension_decoders";

    public static final String ACTION_VIEW = "com.google.android.exoplayer.demo.action.VIEW";
    public static final String EXTENSION_EXTRA = "extension";

    public static final String ACTION_VIEW_LIST =
            "com.google.android.exoplayer.demo.action.VIEW_LIST";
    public static final String URI_LIST_EXTRA = "uri_list";
    public static final String EXTENSION_LIST_EXTRA = "extension_list";

    private String savedStation;

    private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();
    private static final CookieManager DEFAULT_COOKIE_MANAGER;

    static {
        DEFAULT_COOKIE_MANAGER = new CookieManager();
        DEFAULT_COOKIE_MANAGER.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER);
    }

    private Handler mainHandler;
    private EventLogger eventLogger;
    //  private SimpleExoPlayerView simpleExoPlayerView;
    private LinearLayout debugRootView;
    private TextView debugTextView;
    private Button retryButton;

    private DataSource.Factory mediaDataSourceFactory;
    private SimpleExoPlayer player;
    private DefaultTrackSelector trackSelector;
    private TrackSelectionHelper trackSelectionHelper;
    private DebugTextViewHelper debugViewHelper;
    private boolean playerNeedsSource;

    private boolean shouldAutoPlay;
    private int resumeWindow;
    private long resumePosition;
    private Intent intent;


    /* Define log tag */
    private static final String LOG_TAG = PlayerService.class.getSimpleName();


    private static Station mStation;
    private static MediaSessionCompat mSession;
    private int mStationID;
    private int mStationIDCurrent;
    private int mStationIDLast;
    private String mStationMetadata;
    private String mStreamUri;
    private boolean mPlayback;
    private boolean mStationLoading;
    private boolean mStationMetadataReceived;
    private int mPlayerInstanceCounter;
    private int mReconnectCounter;
    private WifiManager.WifiLock mWifiLock;
    private MyRadio myRadio;


    /* Constructor (default) */
    public PlayerService() {
    }

    /* Getter for current station */
    public static Station getStation() {
        return mStation;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // load app state
        loadAppState(getApplication());

        // set up variables
//        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//        mMediaPlayer = null;
        mPlayerInstanceCounter = 0;
        mReconnectCounter = 0;
        mStationMetadataReceived = false;
        myRadio = MyRadio.getInstance();

        // create Wifi lock
        mWifiLock = ((WifiManager) getSystemService(Context.WIFI_SERVICE)).createWifiLock(WifiManager.WIFI_MODE_FULL, "Transistor_lock");

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        shouldAutoPlay = true;
        mediaDataSourceFactory = buildDataSourceFactory(true);
        mainHandler = new Handler();
        if (CookieHandler.getDefault() != DEFAULT_COOKIE_MANAGER) {
            CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER);
        }

        this.intent = intent;
        Log.e("test", "Onlly test");

        releasePlayer();


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 5000ms
//                showToast("stop player");
                if (player != null) {
//                    releasePlayer();
                }
            }
        }, 10000);


        // checking for empty intent
        if (intent == null) {
            LogHelper.v(LOG_TAG, "Null-Intent received. Stopping self.");
            stopForeground(true); // Remove notification
            stopSelf();
        }

        // ACTION PLAY
        else if (intent.getAction().equals(TransistorKeys.ACTION_PLAY)) {
            LogHelper.v(LOG_TAG, "Service received command: PLAY");

            // get URL of station from intent
            if (intent.hasExtra(TransistorKeys.EXTRA_STATION)) {
                mStation = intent.getParcelableExtra(TransistorKeys.EXTRA_STATION);
                mStationID = intent.getIntExtra(TransistorKeys.EXTRA_STATION_ID, 0);
                mStreamUri = mStation.getStreamUri().toString();
            }


            // update controller - start playback
//            mController.getTransportControls().play();
//            myRadio.startRadio(mStation, startId);
            initializePlayer(mStation);

            Intent i = new Intent();
            i.setAction(TransistorKeys.ACTION_PLAYBACK_STATE_CHANGED);
            i.putExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE, TransistorKeys.PLAYBACK_STARTED);
            i.putExtra(TransistorKeys.EXTRA_STATION, mStation);
            i.putExtra(TransistorKeys.EXTRA_STATION_ID, mStationID);
            LocalBroadcastManager.getInstance(this.getApplication()).sendBroadcast(i);

            mStationLoading = false;
            startPlayback();
            NotificationHelper.update(mStation, mStationID, mStationMetadata, mSession);

        }

        // ACTION STOP
        else if (intent.getAction().equals(TransistorKeys.ACTION_STOP)) {
            LogHelper.v(LOG_TAG, "Service received command: STOP");

            // update controller - pause playback
//            mController.getTransportControls().pause();
//            myRadio.stopRadio();
            releasePlayer();
            stopPlayback(true);
        }

        // ACTION DISMISS
        else if (intent.getAction().equals(TransistorKeys.ACTION_DISMISS)) {
            LogHelper.v(LOG_TAG, "Service received command: DISMISS");

            // update controller - stop playback
//            mController.getTransportControls().stop();
//            myRadio.stopRadio();
            stopPlayback(true);
            releasePlayer();
        }

        // listen for media button
        MediaButtonReceiver.handleIntent(mSession, intent);

        // default return value for media playback
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        LogHelper.v(LOG_TAG, "onDestroy called.");

        // save state
        mPlayback = false;
        savedStation = "Choose a station below to begin..";
        saveAppState();

        // release media session
//        mSession.release();

        // cancel notification
        stopForeground(true);
    }

    /* Starts playback */
    private void startPlayback() {

        // set and save state
        mStationMetadata = mStation.getStationName();
        mStationMetadataReceived = false;
        mStation.setPlaybackState(true);
        mPlayback = true;
        mStationLoading = false;
        mStationIDLast = mStationIDCurrent;
        mStationIDCurrent = mStationID;
        savedStation = mStation.getStationName();
        saveAppState();

        // acquire Wifi lock
        if (!mWifiLock.isHeld()) {
            mWifiLock.acquire();
        }


        // send local broadcast
        Intent i = new Intent();
        i.setAction(TransistorKeys.ACTION_PLAYBACK_STATE_CHANGED);
        i.putExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE, TransistorKeys.PLAYBACK_LOADING_STATION);
        i.putExtra(TransistorKeys.EXTRA_STATION, mStation);
        i.putExtra(TransistorKeys.EXTRA_STATION_ID, mStationID);
        LocalBroadcastManager.getInstance(this.getApplication()).sendBroadcast(i);

        // increase counter
        mPlayerInstanceCounter++;

        // stop running player - request focus and initialize media player
//        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
//            releaseMediaPlayer();
//            NotificationHelper.stop();
//        }
//        if (mStreamUri != null && requestFocus()) {
        if (mStreamUri != null) {
//            initializeMediaPlayer();

            // update MediaSession
//            mSession.setPlaybackState(getPlaybackState());
//            mSession.setMetadata(getMetadata(getApplicationContext(), mStation, mStationMetadata));
//            mSession.setActive(true);

            // put up notification
            NotificationHelper.show(this, mSession, mStation, mStationID, this.getString(R.string.descr_station_stream_loading));

        }


    }


    /* Stops playback */
    private void stopPlayback(boolean dismissNotification) {


        if (mStation == null) {
            return;
        }
        // set and save state
        mStationMetadata = mStation.getStationName();
        mStationMetadataReceived = false;
        mStation.setPlaybackState(false);
        mPlayback = false;
        mStationLoading = false;
        mStationIDLast = mStationID;
        mStationIDCurrent = -1;
        savedStation = "Choose a station below to begin..";
        saveAppState();

        // release Wifi lock
        if (mWifiLock.isHeld()) {
            mWifiLock.release();
        }

        // send local broadcast
        Intent i = new Intent();
        i.setAction(TransistorKeys.ACTION_PLAYBACK_STATE_CHANGED);
        i.putExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE, TransistorKeys.PLAYBACK_STOPPED);
        i.putExtra(TransistorKeys.EXTRA_STATION, mStation);
        i.putExtra(TransistorKeys.EXTRA_STATION_ID, mStationID);
        LocalBroadcastManager.getInstance(this.getApplication()).sendBroadcast(i);

        // reset counter
        mPlayerInstanceCounter = 0;

//
// rmyRadio.stopRadio();
        releasePlayer();


        if (dismissNotification) {
            // dismiss notification
            NotificationHelper.stop();
            // set media session in-active
//            mSession.setActive(false);
        } else {
            // update notification
            NotificationHelper.update(mStation, mStationID, mStation.getStationName(), mSession);
            // keep media session active
//            mSession.setActive(true);
        }

    }


    /* Release the media player */
    private void releaseMediaPlayer() {


//        myRadio.stopRadio();
        releasePlayer();

    }


    /* Creates the metadata needed for MediaSession */
    private MediaMetadataCompat getMetadata(Context context, Station station, String metaData) {
        Bitmap stationImage;

        if (station.getStationImage() != null) {
            // use station image
//            stationImage = BitmapFactory.decodeFile(station.getStationImageFile().toString());
            stationImage = station.getStationImage();
        } else {
            stationImage = null;
        }


        // use name of app as album title
        String albumTitle = context.getResources().getString(R.string.app_name);

        // log metadata change
        LogHelper.i(LOG_TAG, "New Metadata available. Artist: " + station.getStationName() + ", Title: " + metaData + ", Album: " + albumTitle);

        return new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, station.getStationName())
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, metaData)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, albumTitle)
                .putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, stationImage)
                .build();
    }


    /* Saves state of playback */
    private void saveAppState() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplication());
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(TransistorKeys.PREF_STATION_ID_CURRENTLY_PLAYING, mStationIDCurrent);
        editor.putInt(TransistorKeys.PREF_STATION_ID_LAST, mStationIDLast);
        editor.putBoolean(TransistorKeys.PREF_PLAYBACK, mPlayback);
        editor.putString(TransistorKeys.PREF_STATION_NAME_CURRENTLY_PLAYING, savedStation);
        editor.putBoolean(TransistorKeys.PREF_STATION_LOADING, mStationLoading);
        editor.putString(TransistorKeys.PREF_STATION_METADATA, mStationMetadata);
        editor.apply();
        LogHelper.v(LOG_TAG, "Saving state (" + mStationIDCurrent + " / " + mStationIDLast + " / " + mPlayback + " / " + mStationLoading + " / " + ")");
    }


    /* Loads app state from preferences */
    private void loadAppState(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        mStationIDCurrent = settings.getInt(TransistorKeys.PREF_STATION_ID_CURRENTLY_PLAYING, -1);
        mStationIDLast = settings.getInt(TransistorKeys.PREF_STATION_ID_LAST, -1);
        LogHelper.v(LOG_TAG, "Loading state (" + mStationIDCurrent + " / " + mStationIDLast + ")");
    }


    //// axo player

    private void releasePlayer() {
        if (player != null) {
//            debugViewHelper.stop();
//            debugViewHelper = null;
            shouldAutoPlay = player.getPlayWhenReady();
//      updateResumePosition();
            player.release();
            player = null;
            trackSelector = null;
            trackSelectionHelper = null;
            eventLogger = null;
        }
    }


    @Override
    public void onVisibilityChange(int visibility) {
//        debugRootView.setVisibility(visibility);
    }


    // Internal methods

    private void initializePlayer(Station mStation) {
//        Intent intent = getIntent();
        if (player == null) {

            DrmSessionManager<FrameworkMediaCrypto> drmSessionManager = null;
            @SimpleExoPlayer.ExtensionRendererMode int extensionRendererMode =
                    ((MyRadio) getApplication()).useExtensionRenderers()
                            ? (false ? SimpleExoPlayer.EXTENSION_RENDERER_MODE_PREFER
                            : SimpleExoPlayer.EXTENSION_RENDERER_MODE_ON)
                            : SimpleExoPlayer.EXTENSION_RENDERER_MODE_OFF;
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveVideoTrackSelection.Factory(BANDWIDTH_METER);
            trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            trackSelectionHelper = new TrackSelectionHelper(trackSelector, videoTrackSelectionFactory);
            player = ExoPlayerFactory.newSimpleInstance(this, trackSelector, new DefaultLoadControl(),
                    drmSessionManager, extensionRendererMode);
            player.addListener(this);

            eventLogger = new EventLogger(trackSelector);
            player.addListener(eventLogger);
            player.setAudioDebugListener(eventLogger);
            player.setVideoDebugListener(eventLogger);
            player.setMetadataOutput(eventLogger);

//      simpleExoPlayerView.setPlayer(player);
            player.setPlayWhenReady(shouldAutoPlay);
//            debugViewHelper = new DebugTextViewHelper(player, debugTextView);
//            debugViewHelper.start();
            playerNeedsSource = true;
        }
        if (playerNeedsSource) {
            if (intent != null) {
                String action = intent.getAction();
                Uri[] uris;
                String[] extensions;

                extensions = new String[]{
                        mStation.getStreamUri().toString()};
              /*  if (ACTION_VIEW.equals(action)) {
                    uris = new Uri[]{intent.getData()};
                    extensions = new String[]{intent.getStringExtra(EXTENSION_EXTRA)};
                } else if (ACTION_VIEW_LIST.equals(action)) {
                    String[] uriStrings = intent.getStringArrayExtra(URI_LIST_EXTRA);
                    uris = new Uri[uriStrings.length];
                    for (int i = 0; i < uriStrings.length; i++) {
                        uris[i] = Uri.parse(uriStrings[i]);
                    }
                    extensions = intent.getStringArrayExtra(EXTENSION_LIST_EXTRA);
                    if (extensions == null) {
                        extensions = new String[uriStrings.length];
                    }
                } else {
                    showToast(getString(R.string.unexpected_intent_action, action));
                    return;
                }*/
//            if (Util.maybeRequestReadExternalStoragePermission(RadioApp.getInstance(), uris)) {
//                // The player will be reinitialized if the permission is granted.
//                return;
//            }
                MediaSource[] mediaSources = new MediaSource[1];

                mediaSources[0] = buildMediaSource(mStation.getStreamUri(), null);

//                MediaSource[] mediaSources = new MediaSource[uris.length];
//                for (int i = 0; i < uris.length; i++) {
//                    mediaSources[i] = buildMediaSource(uris[i], extensions[i]);
//                }
                MediaSource mediaSource = mediaSources.length == 1 ? mediaSources[0]
                        : new ConcatenatingMediaSource(mediaSources);
                boolean haveResumePosition = resumeWindow != C.INDEX_UNSET;
                if (haveResumePosition) {
                    player.seekTo(resumeWindow, resumePosition);
                }
                player.prepare(mediaSource, !haveResumePosition, false);
                playerNeedsSource = false;
                updateButtonVisibilities();
            }
        }
    }


    private MediaSource buildMediaSource(Uri uri, String overrideExtension) {
        int type = Util.inferContentType(!TextUtils.isEmpty(overrideExtension) ? "." + overrideExtension
                : uri.getLastPathSegment());
        switch (type) {
            case C.TYPE_SS:
                return new SsMediaSource(uri, buildDataSourceFactory(false),
                        new DefaultSsChunkSource.Factory(mediaDataSourceFactory), mainHandler, eventLogger);
            case C.TYPE_DASH:
                return new DashMediaSource(uri, buildDataSourceFactory(false),
                        new DefaultDashChunkSource.Factory(mediaDataSourceFactory), mainHandler, eventLogger);
            case C.TYPE_HLS:
                return new HlsMediaSource(uri, mediaDataSourceFactory, mainHandler, eventLogger);
            case C.TYPE_OTHER:
                return new ExtractorMediaSource(uri, mediaDataSourceFactory, new DefaultExtractorsFactory(),
                        mainHandler, eventLogger);
            default: {
                throw new IllegalStateException("Unsupported type: " + type);
            }
        }
    }


    /**
     * Returns a new DataSource factory.
     *
     * @param useBandwidthMeter Whether to set {@link #BANDWIDTH_METER} as a listener to the new
     *                          DataSource factory.
     * @return A new DataSource factory.
     */
    private DataSource.Factory buildDataSourceFactory(boolean useBandwidthMeter) {
        return ((MyRadio) getApplication())
                .buildDataSourceFactory(useBandwidthMeter ? BANDWIDTH_METER : null);
    }

    /**
     * Returns a new HttpDataSource factory.
     *
     * @param useBandwidthMeter Whether to set {@link #BANDWIDTH_METER} as a listener to the new
     *                          DataSource factory.
     * @return A new HttpDataSource factory.
     */
    private HttpDataSource.Factory buildHttpDataSourceFactory(boolean useBandwidthMeter) {
        return ((MyRadio) getApplication())
                .buildHttpDataSourceFactory(useBandwidthMeter ? BANDWIDTH_METER : null);
    }

    // ExoPlayer.EventListener implementation

    @Override
    public void onLoadingChanged(boolean isLoading) {
        // Do nothing.
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == ExoPlayer.STATE_ENDED) {
//      showControls();
        }
        updateButtonVisibilities();
    }

    @Override
    public void onPositionDiscontinuity() {
        if (playerNeedsSource) {
            // This will only occur if the user has performed a seek whilst in the error state. Update the
            // resume position so that if the user then retries, playback will resume from the position to
            // which they seeked.
//      updateResumePosition();
        }
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {
        // Do nothing.
    }

    @Override
    public void onPlayerError(ExoPlaybackException e) {
        String errorString = null;
        if (e.type == ExoPlaybackException.TYPE_RENDERER) {
            Exception cause = e.getRendererException();
            if (cause instanceof MediaCodecRenderer.DecoderInitializationException) {
                // Special case for decoder initialization failures.
                MediaCodecRenderer.DecoderInitializationException decoderInitializationException =
                        (MediaCodecRenderer.DecoderInitializationException) cause;
                if (decoderInitializationException.decoderName == null) {
                    if (decoderInitializationException.getCause() instanceof MediaCodecUtil.DecoderQueryException) {
                        errorString = getString(R.string.error_querying_decoders);
                    } else if (decoderInitializationException.secureDecoderRequired) {
                        errorString = getString(R.string.error_no_secure_decoder,
                                decoderInitializationException.mimeType);
                    } else {
                        errorString = getString(R.string.error_no_decoder,
                                decoderInitializationException.mimeType);
                    }
                } else {
                    errorString = getString(R.string.error_instantiating_decoder,
                            decoderInitializationException.decoderName);
                }
            }
        }
        if (errorString != null) {
            showToast(errorString);
        }
        playerNeedsSource = true;
        if (isBehindLiveWindow(e)) {
//      clearResumePosition();
            initializePlayer(mStation);
        } else {
//      updateResumePosition();
            updateButtonVisibilities();
//            showControls();
        }
    }


    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
        updateButtonVisibilities();
        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
        if (mappedTrackInfo != null) {
            if (mappedTrackInfo.getTrackTypeRendererSupport(C.TRACK_TYPE_VIDEO)
                    == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                showToast(R.string.error_unsupported_video);
            }
            if (mappedTrackInfo.getTrackTypeRendererSupport(C.TRACK_TYPE_AUDIO)
                    == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                showToast(R.string.error_unsupported_audio);
            }
        }
    }

    private void showToast(int messageId) {
        showToast(getString(messageId));
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private static boolean isBehindLiveWindow(ExoPlaybackException e) {
        if (e.type != ExoPlaybackException.TYPE_SOURCE) {
            return false;
        }
        Throwable cause = e.getSourceException();
        while (cause != null) {
            if (cause instanceof BehindLiveWindowException) {
                return true;
            }
            cause = cause.getCause();
        }
        return false;
    }

    // User controls

    private void updateButtonVisibilities() {
//        debugRootView.removeAllViews();
//        showToast("Playing");
        if (playerNeedsSource) {
            initializePlayer(mStation);
            showToast("Retrying to load this station!");
        }
//        retryButton.setVisibility(playerNeedsSource ? View.VISIBLE : View.GONE);
//    debugRootView.addView(retryButton);

        if (player == null) {
            return;
        }

        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
        if (mappedTrackInfo == null) {
            return;
        }

    }


}
