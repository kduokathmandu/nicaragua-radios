/**
 * CollectionAdapter.java
 * Implements the CollectionAdapter class
 * A CollectionAdapter is a custom adapter for a RecyclerView
 * <p>
 * This file is part of
 * TRANSISTOR - Radio App for Android
 * <p>
 * Copyright (c) 2015-16 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package com.nicaragua.kduo;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.nicaragua.kduo.helpers.ImageHelper;
import com.nicaragua.kduo.helpers.TransistorKeys;
import com.nicaragua.kduo.core.Station;
import com.nicaragua.kduo.helpers.LogHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * CollectionAdapter class
 */
public final class CollectionAdapterBackup extends RecyclerView.Adapter<CollectionAdapterViewHolder> implements Filterable {


    /* Define log tag */
    private static final String LOG_TAG = CollectionAdapterBackup.class.getSimpleName();
    /* Main class variables */
    private final Activity mActivity;
    private SharedPreferences prefs;
    //    private final File mFolder;
    private BroadcastReceiver mPlaybackStateChangedReceiver;
    private boolean mPlayback;
    private boolean mStationLoading;
    private int mStationIDCurrent;
    private int mStationIDLast;
    private int mStationIDSelected;
    private boolean mTwoPane;
    //    private final SortedList<Station> mStationList;
    private ArrayList<Station> stations;
    private ArrayList<Station> data;


    /* Constructor */
    public CollectionAdapterBackup(Activity activity, ArrayList<Station> stations) {
        // set main variables
        mActivity = activity;
//        mFolder = folder;
        this.stations = stations;
        mStationIDSelected = 0;
        prefs = mActivity.getSharedPreferences("com.suman.radio.app.tms", Context.MODE_PRIVATE);
 /*       mStationList = new SortedList<Station>(Station.class, new SortedListAdapterCallback<Station>(this) {

            @Override
            public int compare(Station station1, Station station2) {
                // Compares two stations: returns "1" if name if this station is greater than name of given station
                return station1.getStationName().compareToIgnoreCase(station2.getStationName());
            }

            @Override
            public boolean areContentsTheSame(Station oldStation, Station newStation) {
                return oldStation.getStationName().equals(newStation.getStationName());
            }

            @Override
            public boolean areItemsTheSame(Station station1, Station station2) {
                // return station1.equals(station2);
                return areContentsTheSame(station1, station2);
            }
        });*/


        // load state
        // loadAppState(mActivity);

    }

    public void filterFavourites(boolean showFavourites) {

        if (data == null) {
            data = stations;
        } else {
            stations = data;
        }
        if (showFavourites) {
            stations = new ArrayList<>();
            for (Station station : data) {
                if (station.getCustomStation().isFavourite()) {
                    stations.add(station);
                }
            }
            if (stations.size() == 0)
                Toast.makeText(mActivity, "No Favourites added!", Toast.LENGTH_SHORT).show();
        }
        notifyDataSetChanged();

    }

    public void sortStations() {
        Collections.reverse(stations);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<Station> results = new ArrayList<Station>();
                if (data == null)
                    data = stations;
                if (constraint != null) {
                    if (data != null & data.size() > 0) {
                        for (final Station g : data) {
                            if (g.getStationName().toLowerCase().contains(constraint.toString().toLowerCase()))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                stations = (ArrayList<Station>) results.values;
                notifyDataSetChanged();

            }
        };
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        // load state
        loadAppState(mActivity);
        // initialize broadcast receivers
        initializeBroadcastReceivers();
    }


    @Override
    public CollectionAdapterViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {

        // get view
        View v = LayoutInflater.from(parent.getContext()).inflate(com.nicaragua.kduo.R.layout.list_item_collection, parent, false);

        // put view into holder and return
        return new CollectionAdapterViewHolder(v);
    }


    @Override
    public void onBindViewHolder(CollectionAdapterViewHolder holder, final int position) {
        // final int position --> Do not treat position as fixed; only use immediately and call holder.getAdapterPosition() to look it up later
        // get station from position

        final Station station = stations.get(position);


        if (mTwoPane && mStationIDSelected == position) {
            holder.getListItemLayout().setSelected(true);
        } else {
            holder.getListItemLayout().setSelected(false);
        }

        // create station image
        Bitmap stationImageSmall;
        Bitmap stationImage;
//        if (station.getStationImageFile().exists()) {
//            // get image from collection
//            stationImageSmall = BitmapFactory.decodeFile(station.getStationImageFile().toString());
//        } else {
//            stationImageSmall = null;
//
//  }

        stationImageSmall = station.getStationImage();

        ImageHelper imageHelper = new ImageHelper(stationImageSmall, mActivity);
        stationImage = imageHelper.createCircularFramedImage(192, com.nicaragua.kduo.R.color.transistor_grey_lighter);

        // set station image
//        holder.getStationImageView().setImageBitmap(stationImage);
        Picasso.with(mActivity).load(station.getStationImageURL()).fit().placeholder(com.nicaragua.kduo.R.drawable.progress_animaton).into(holder.getStationImageView());
//        holder.getStationImageView()

        // set station name
        holder.getStationNameView().setText(station.getStationName());

        // set playback indicator - in phone view only
//        if (!mTwoPane && mPlayback && (station.getPlaybackState() || mStationIDCurrent == position)) {
        if (!mTwoPane && mPlayback && (station.getPlaybackState())) {
            if (mStationLoading) {
                holder.getPlaybackIndicator().setBackgroundResource(com.nicaragua.kduo.R.drawable.ic_playback_indicator_small_loading_24dp);
            } else {
                holder.getPlaybackIndicator().setBackgroundResource(com.nicaragua.kduo.R.drawable.ic_playback_indicator_small_started_24dp);
            }
            holder.getPlaybackIndicator().setVisibility(View.VISIBLE);
            prefs.edit().putString(MainActivityFragment.CURRENT_RADIO, station.getStationName()).apply();
        } else {
            holder.getPlaybackIndicator().setVisibility(View.GONE);
        }

        holder.getStationFavouriteView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (station.getCustomStation().isFavourite()) {
                    removeFavoriteStation(mActivity, station.getStationName());
//                    stations.get(stations.indexOf(station)).getCustomStation().setFavourite(false);
                    station.getCustomStation().setFavourite(false);
                } else {
                    saveFavoriteStation(mActivity, station.getStationName());
                    station.getCustomStation().setFavourite(true);
//                    stations.get(stations.indexOf(station)).getCustomStation().setFavourite(true);
                }
                notifyDataSetChanged();
            }
        });

        if (station.getCustomStation().isFavourite()) {
            holder.getStationFavouriteView().setImageDrawable(ContextCompat.getDrawable(mActivity, com.nicaragua.kduo.R.drawable.favourite_true));
        } else {
            holder.getStationFavouriteView().setImageDrawable(ContextCompat.getDrawable(mActivity, com.nicaragua.kduo.R.drawable.favourite_false));
        }

        // attach three dots menu - in phone view only
//        if (!mTwoPane) {
//            holder.getStationMenuView().setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    StationContextMenu menu = new StationContextMenu();
//                    menu.initialize(mActivity, view, station, position);
//                    menu.show();
//                }
//            });
//        } else {
//            holder.getStationMenuView().setVisibility(View.GONE);
//        }

        // attach click listener
        holder.setClickListener(new CollectionAdapterViewHolder.ClickListener() {
            @Override
            public void onClick(View view, int pos, boolean isLongClick) {
                mStationIDSelected = pos;
                saveAppState(mActivity);
                LogHelper.v(LOG_TAG, "Selected station (ID): " + mStationIDSelected);
                handleSingleClick(pos, stations.get(pos));

            }
        });

    }


    private void removeFavoriteStation(Context context, String stationName) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        String tempPlayList = settings.getString(TransistorKeys.PREF_FAVORITE_STATION, "");
        stationName = stationName + ",";
        String finalPlaylist = tempPlayList.replaceAll(stationName, "");

        SharedPreferences.Editor editor = settings.edit();
        editor.putString(TransistorKeys.PREF_FAVORITE_STATION, finalPlaylist);
        editor.apply();
    }


    public void saveFavoriteStation(Context context, String stationName) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        StringBuilder sb = new StringBuilder();
        String tempPlayList = settings.getString(TransistorKeys.PREF_FAVORITE_STATION, "");
        sb.append(tempPlayList);
        if (!sb.toString().toLowerCase().contains(stationName.toLowerCase())) {
            sb.append(stationName).append(",");
        }

        SharedPreferences.Editor editor = settings.edit();
        editor.putString(TransistorKeys.PREF_FAVORITE_STATION, sb.toString());
        editor.apply();

    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return stations.size();
    }


    /* Handles click on list item */
    private void handleSingleClick(int position, Station station) {

//        Station station = stations.get(position);
        if (mTwoPane) {
            Bundle args = new Bundle();
            args.putParcelable(TransistorKeys.ARG_STATION, station);
            args.putInt(TransistorKeys.ARG_STATION_ID, position);
            args.putBoolean(TransistorKeys.ARG_TWO_PANE, mTwoPane);

            PlayerActivityFragment playerActivityFragment = new PlayerActivityFragment();
            playerActivityFragment.setArguments(args);
            mActivity.getFragmentManager().beginTransaction()
                    .replace(com.nicaragua.kduo.R.id.player_container, playerActivityFragment, TransistorKeys.PLAYER_FRAGMENT_TAG)
                    .commit();
        } else {


            // add ID of station to intent and start activity
            Intent intent = new Intent(mActivity, PlayerActivity.class);
            intent.setAction(TransistorKeys.ACTION_SHOW_PLAYER);
            intent.putExtra(TransistorKeys.EXTRA_STATION, station);
            intent.putExtra(TransistorKeys.EXTRA_STATION_ID, position);
            mActivity.startActivity(intent);
        }


    }


    /* Handles long click on list item */
    private void handleLongClick(int position) {

        // get current playback state
        loadAppState(mActivity);

        if (mPlayback && stations.get(position).getPlaybackState()) {
            // stop player service using intent
            Intent intent = new Intent(mActivity, PlayerService.class);
            intent.setAction(TransistorKeys.ACTION_STOP);
            mActivity.startService(intent);
            LogHelper.v(LOG_TAG, "Stopping player service.");

            // keep track of playback state
            mStationIDLast = position;
            mStationIDCurrent = -1;

            // remove playback flag from this station
            mPlayback = false;
            stations.get(position).setPlaybackState(false);

            // inform user
            Toast.makeText(mActivity, mActivity.getString(com.nicaragua.kduo.R.string.toastmessage_long_press_playback_stopped), Toast.LENGTH_LONG).show();
        } else {
            // start player service using intent
            Intent intent = new Intent(mActivity, PlayerService.class);
            intent.setAction(TransistorKeys.ACTION_PLAY);
            intent.putExtra(TransistorKeys.EXTRA_STATION, stations.get(position));
            intent.putExtra(TransistorKeys.EXTRA_STATION_ID, position);
            mActivity.startService(intent);
            LogHelper.v(LOG_TAG, "Starting player service.");

            // keep track of playback state
            mStationIDLast = mStationIDCurrent;
            mStationIDCurrent = position;

            // remove playback flag from last station
            if (mPlayback && mStationIDLast != -1) {
                stations.get(mStationIDLast).setPlaybackState(false);
            }
            // add playback flag to current station
            mPlayback = true;
            stations.get(mStationIDCurrent).setPlaybackState(true);

            // inform user
            Toast.makeText(mActivity, mActivity.getString(com.nicaragua.kduo.R.string.toastmessage_long_press_playback_started), Toast.LENGTH_LONG).show();
        }

        // vibrate 50 milliseconds
        Vibrator v = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(50);

        // save app state
        saveAppState(mActivity);

    }


    /* Loads app state from preferences */
    private void loadAppState(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        mTwoPane = settings.getBoolean(TransistorKeys.PREF_TWO_PANE, false);
        mStationIDCurrent = settings.getInt(TransistorKeys.PREF_STATION_ID_CURRENTLY_PLAYING, -1);
        mStationIDLast = settings.getInt(TransistorKeys.PREF_STATION_ID_LAST, -1);
        mStationIDSelected = settings.getInt(TransistorKeys.PREF_STATION_ID_SELECTED, 0);
        mPlayback = settings.getBoolean(TransistorKeys.PREF_PLAYBACK, false);
        mStationLoading = settings.getBoolean(TransistorKeys.PREF_STATION_LOADING, false);
        LogHelper.v(LOG_TAG, "Loading state (" + mStationIDCurrent + " / " + mStationIDLast + " / " + mPlayback + " / " + mStationLoading + ")");
    }


    /* Saves app state to SharedPreferences */
    private void saveAppState(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(TransistorKeys.PREF_STATION_ID_SELECTED, mStationIDSelected);
        editor.apply();
        LogHelper.v(LOG_TAG, "Saving state (" + mStationIDCurrent + " / " + mStationIDLast + " / " + mPlayback + " / " + mStationLoading + " / " + mStationIDSelected + ")");
    }

/*

    */
/* Setter for image file within station object with given ID *//*

    public void setNewImageFile(int stationID, File stationImageFile) {
        mStationList.get(stationID).setStationImageFile(stationImageFile);
    }
*/


    /* Setter for ID of currently selected station */
    public void setStationIDSelected(int stationIDSelected, boolean playbackState, boolean startPlayback) {
        mStationIDSelected = stationIDSelected;
        saveAppState(mActivity);
        if (mStationIDSelected >= 0 && mStationIDSelected < stations.size()) {
            stations.get(stationIDSelected).setPlaybackState(playbackState);
        }
        if (mTwoPane && mStationIDSelected >= 0 && mStationIDSelected < stations.size()) {
            handleSingleClick(mStationIDSelected,stations.get(mStationIDSelected));
        }

        if (startPlayback) {
            // start player service using intent
            Intent intent = new Intent(mActivity, PlayerService.class);
            intent.setAction(TransistorKeys.ACTION_PLAY);
            intent.putExtra(TransistorKeys.EXTRA_STATION, stations.get(stationIDSelected));
            intent.putExtra(TransistorKeys.EXTRA_STATION_ID, stationIDSelected);
            mActivity.startService(intent);
            LogHelper.v(LOG_TAG, "Starting player service.");
        }
    }


    /* Setter for two pane flag */
    public void setTwoPane(boolean twoPane) {
        mTwoPane = twoPane;
    }


    /* Reloads app state */
    public void refresh() {
        loadAppState(mActivity);
    }


    /* Finds station when given its Uri */
    public Station findStation(Uri streamUri) {

        // traverse list of stations
        for (int i = 0; i < stations.size(); i++) {
            Station station = stations.get(i);
            if (station.getStreamUri().equals(streamUri)) {
                return station;
            }
        }

        // return null if nothing was found
        return null;
    }


    /* Getter for ID of given station */
    public int getStationID(Station station) {
        return stations.indexOf(station);
    }


    /* Getter for station of given ID */
    public Station getStation(int stationID) {
        return stations.get(stationID);
    }

/*

    */
/* Add station to collection *//*

    public int add(Station station) {
        if (station.getStationName() != null && station.getStreamUri() != null) {
            // add station to list of stations
            mStationList.add(station);

            // save playlist file and image file to local storage
            station.writePlaylistFile(mFolder);
            if (station.getStationImage() != null) {
                station.writeImageFile();
            }

            // return new index
            return mStationList.indexOf(station);
        } else {
            // notify user and log failure to add
            String errorTitle = mActivity.getResources().getString(R.string.dialog_error_title_fetch_write);
            String errorMessage = mActivity.getResources().getString(R.string.dialog_error_message_fetch_write);
            String errorDetails = mActivity.getResources().getString(R.string.dialog_error_details_write);
            DialogError dialogError = new DialogError(mActivity, errorTitle, errorMessage, errorDetails);
            dialogError.show();
            LogHelper.e(LOG_TAG, "Unable to add station to collection: Duplicate name and/or stream URL.");
            return -1;
        }
    }
*/

/*

    */
/* Rename station within collection *//*

    public int rename(String newStationName, Station station, int stationID) {

        // get old station
        Station oldStation = mStationList.get(stationID);

        // name of station is new
        if (station != null && !oldStation.getStationName().equals(newStationName)) {

            // get reference to old files
            File oldStationPlaylistFile = oldStation.getStationPlaylistFile();
            File oldStationImageFile = oldStation.getStationImageFile();

            // update new station
            station.setStationName(newStationName);
            station.setStationPlaylistFile(mFolder);
            station.setStationImageFile(mFolder);

            // rename playlist file
            oldStationPlaylistFile.delete();
            station.writePlaylistFile(mFolder);

            // rename image file
            oldStationImageFile.renameTo(station.getStationImageFile());

            // update station list
            mStationList.updateItemAt(stationID, station);

            // return changed station
            return mStationList.indexOf(station);

        } else {
            // name of station is null or not new - notify user
            Toast.makeText(mActivity, mActivity.getString(R.string.toastalert_rename_unsuccessful), Toast.LENGTH_LONG).show();
            return -1;
        }

    }


    */
/* Delete station within collection *//*

    public int delete(Station station, int stationID) {

        boolean success = false;

        // delete playlist file
        File stationPlaylistFile = station.getStationPlaylistFile();
        if (stationPlaylistFile.exists()) {
            stationPlaylistFile.delete();
            success = true;
        }

        // delete station image file
        File stationImageFile = station.getStationImageFile();
        if (stationImageFile.exists()) {
            stationImageFile.delete();
            success = true;
        }

        // remove station and notify user
        if (success) {
            mStationList.removeItemAt(stationID);
            Toast.makeText(mActivity, mActivity.getString(R.string.toastalert_delete_successful), Toast.LENGTH_LONG).show();
        }

        // delete station shortcut
        ShortcutHelper shortcutHelper = new ShortcutHelper(mActivity);
        shortcutHelper.removeShortcut(station);

        if (mTwoPane) {
            // determine ID of next station to display in two pane mode
            if (mStationList.size() >= stationID) {
                stationID--;
            }

            if (stationID >= 0) {
                // show next station
                Bundle args = new Bundle();
                args.putParcelable(TransistorKeys.ARG_STATION, mStationList.get(stationID));
                args.putInt(TransistorKeys.ARG_STATION_ID, stationID);
                args.putBoolean(TransistorKeys.ARG_TWO_PANE, mTwoPane);
                PlayerActivityFragment playerActivityFragment = new PlayerActivityFragment();
                playerActivityFragment.setArguments(args);
                mActivity.getFragmentManager().beginTransaction()
                        .replace(R.id.player_container, playerActivityFragment, TransistorKeys.PLAYER_FRAGMENT_TAG)
                        .commit();
            }
        }

        // return ID of next station
        return stationID;
    }
*/


    /* Handles changes in state of playback, eg. start, stop, loading stream */
    private void handlePlaybackStateChanged(Intent intent) {

        // load app state
        loadAppState(mActivity);

        if (intent.hasExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE) && intent.hasExtra(TransistorKeys.EXTRA_STATION_ID)) {

            notifyDataSetChanged();

            // get station ID from intent
            int stationID = intent.getIntExtra(TransistorKeys.EXTRA_STATION_ID, 0);
            switch (intent.getIntExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE, 1)) {

                // CASE: player is preparing stream
                case TransistorKeys.PLAYBACK_LOADING_STATION:
                    if (mStationIDLast > -1 && mStationIDLast < stations.size()) {
                        stations.get(mStationIDLast).setPlaybackState(false);
                    }
                    mStationLoading = true;
                    mPlayback = true;
                    if (stationID > -1 && stationID < stations.size()) {
                        stations.get(stationID).setPlaybackState(true);
                    }
                    notifyDataSetChanged();
                    break;

                // CASE: playback has started
                case TransistorKeys.PLAYBACK_STARTED:
                    mStationLoading = false;
                    if (stationID > -1 && stationID < stations.size()) {
                        stations.get(stationID).setPlaybackState(true);
                    }
                    notifyDataSetChanged();
                    break;

                // CASE: playback was stopped
                case TransistorKeys.PLAYBACK_STOPPED:
                    mPlayback = false;
                    if (stationID > -1 && stationID < stations.size()) {
                        stations.get(stationID).setPlaybackState(false);
                    }
                    notifyDataSetChanged();
                    break;
            }
        }

    }


    /* Initializes broadcast receivers for onCreate */
    private void initializeBroadcastReceivers() {

        // RECEIVER: state of playback has changed
        mPlaybackStateChangedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE)) {
                    handlePlaybackStateChanged(intent);
                }
            }
        };
        IntentFilter playbackStateChangedIntentFilter = new IntentFilter(TransistorKeys.ACTION_PLAYBACK_STATE_CHANGED);
        LocalBroadcastManager.getInstance(mActivity).registerReceiver(mPlaybackStateChangedReceiver, playbackStateChangedIntentFilter);
    }


}