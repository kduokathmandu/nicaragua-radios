/**
 * PlayerActivity.java
 * Implements the app's player activity
 * The player activity sets up the now playing view for phone  mode and inflates a menu bar menu
 * <p>
 * This file is part of
 * TRANSISTOR - Radio App for Android
 * <p>
 * Copyright (c) 2015-16 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package com.nicaragua.kduo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import com.nicaragua.kduo.app.MyRadio;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.nicaragua.kduo.core.Station;
import com.nicaragua.kduo.helpers.TransistorKeys;


/**
 * PlayerActivity class
 */
public final class PlayerActivity extends AppCompatActivity {

    /* Define log tag */
    private static final String LOG_TAG = PlayerActivity.class.getSimpleName();
    private MyRadio myRadio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set content view
        setContentView(com.nicaragua.kduo.R.layout.activity_player);


        myRadio = MyRadio.getInstance();
        showAds();

        // get intent
        Intent intent = getIntent();

        // CASE: show player in phone mode
        if (intent != null && TransistorKeys.ACTION_SHOW_PLAYER.equals(intent.getAction())) {

            // get station from intent
            Station station;
            if (intent.hasExtra(TransistorKeys.EXTRA_STATION)) {
                station = intent.getParcelableExtra(TransistorKeys.EXTRA_STATION);
            } else {
                station = null;
            }

            // get id of station from intent
            int stationID = 0;
            if (intent.hasExtra(TransistorKeys.EXTRA_STATION_ID)) {
                stationID = intent.getIntExtra(TransistorKeys.EXTRA_STATION_ID, 0);
            }

            // get playback action from intent (if started from shortcut)
            boolean startPlayback;
            if (intent.hasExtra(TransistorKeys.EXTRA_PLAYBACK_STATE)) {
                startPlayback = intent.getBooleanExtra(TransistorKeys.EXTRA_PLAYBACK_STATE, false);

                // enable the Up button
                ActionBar actionBar = getSupportActionBar();
                if (actionBar != null) {
                    actionBar.setDisplayHomeAsUpEnabled(true);
                }

            } else {
                startPlayback = false;
            }

            // create bundle for player activity fragment
            Bundle args = new Bundle();
            args.putParcelable(TransistorKeys.ARG_STATION, station);
            args.putInt(TransistorKeys.ARG_STATION_ID, stationID);
            args.putBoolean(TransistorKeys.ARG_PLAYBACK, startPlayback);

            PlayerActivityFragment playerActivityFragment = new PlayerActivityFragment();
            playerActivityFragment.setArguments(args);

            getFragmentManager().beginTransaction()
                    .add(com.nicaragua.kduo.R.id.player_container, playerActivityFragment)
                    .commit();


            myRadio.mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();

                    AdRequest adRequest = new AdRequest.Builder()
                            .build();
                    myRadio.mInterstitialAd.loadAd(adRequest);

                }
            });


        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void showAds() {
        if (myRadio.mInterstitialAd.isLoaded()) {
            myRadio.mInterstitialAd.show();
        }
//
//        if (myRadio.facebookInterstitialAd.isAdLoaded()) {
//            myRadio.facebookInterstitialAd.show();
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.nicaragua.kduo.R.menu.menu_player_actionbar, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public void onBackPressed() {
        try {
            super.onBackPressed();
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        Intent playerIntent = new Intent(this, MainActivity.class);
        startActivity(playerIntent);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // make sure that PlayerActivityFragment's onActivityResult() gets called
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
