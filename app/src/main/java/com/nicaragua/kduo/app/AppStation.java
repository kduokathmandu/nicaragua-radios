package com.nicaragua.kduo.app;

/**
 * Created by suman on 1/15/17.
 */



public class AppStation {

    public String image_url;
    public String radio_title;
    public String stream_url;

    public AppStation() {
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getRadio_title() {
        return radio_title;
    }

    public void setRadio_title(String radio_title) {
        this.radio_title = radio_title;
    }

    public String getStream_url() {
        return stream_url;
    }

    public void setStream_url(String stream_url) {
        this.stream_url = stream_url;
    }
}
