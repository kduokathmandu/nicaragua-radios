package com.nicaragua.kduo.app;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by suman on 1/15/17.
 */

public class CustomStation implements Parcelable {

    @SuppressWarnings("unused")
    public static final Creator<CustomStation> CREATOR = new Creator<CustomStation>() {
        @Override
        public CustomStation createFromParcel(Parcel in) {
            return new CustomStation(in);
        }

        @Override
        public CustomStation[] newArray(int size) {
            return new CustomStation[size];
        }
    };
    private String stationName;
    private String stationUrl;
    private String stationImage;
    private boolean isFavourite;

    public CustomStation(String stationName, String stationUrl, String stationImage) {
        this.stationName = stationName;
        this.stationUrl = stationUrl;
        if (stationUrl != null) {
            String finaUrl = stationUrl.replaceAll("/listen.pls", "");
            this.stationUrl = finaUrl;
        }
        this.stationImage = stationImage;
        this.isFavourite = false;
    }

    public CustomStation(String stationName, String stationUrl, String stationImage, boolean isFavourite) {
        this.stationName = stationName;
        String finaUrl = stationUrl.replaceAll("/listen.pls", "");
        this.stationUrl = finaUrl;
        this.stationImage = stationImage;
        this.isFavourite = isFavourite;
    }

    protected CustomStation(Parcel in) {
        stationName = in.readString();
        stationUrl = in.readString();
        stationImage = in.readString();
        isFavourite = in.readByte() != 0; // true if byte != 0
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(stationName);
        dest.writeString(stationUrl);
        dest.writeString(stationImage);
        dest.writeByte((byte) (isFavourite ? 1 : 0));
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public Uri getStationUrl() {
        return Uri.parse(stationUrl.trim());

    }

    public void setStationUrl(String stationUrl) {
        this.stationUrl = stationUrl;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public String getStationImage() {
        return stationImage;
    }

    public void setStationImage(String stationImage) {
        this.stationImage = stationImage;
    }
}
